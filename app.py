from utils import read_device_data, combine_device_data, preprocessing_data, create_lag_features
from utils import constant_num, classes_name, lag_cols
from flask import Flask, request, jsonify, make_response
import pickle
import pandas as pd
import traceback

app = Flask(__name__)


@app.route('/ping')
def healthcheck():
    return "pong"


detect_activities_model = pickle.load(open('./models/detect_activities_model.pkl', 'rb'))
detect_numofdevices_model = pickle.load(open('./models/detect_running_with_more_devices.pkl', 'rb'))


@app.route('/detect_activities', methods=['GET'])
def detect_activities():
    """input: data of 1 device in json raw
    return: 'std' if that person is standing
            'jog' if that person is jogging
            'wlk' if that person is walking
            'ske' if that person is shaking the device (cheating)
    """
    try:
        data = request.get_json()
        acceler = pd.DataFrame(data['acceler'])
        gyro = pd.DataFrame(data['gyro'])
        # preprocessing
        nbins = constant_num.nbins
        acceler = read_device_data(acceler, nbins)
        gyro = read_device_data(gyro, nbins)
        sensor = combine_device_data(acceler, gyro, nbins)
        preprocess_data = preprocessing_data(sensor)
        df = preprocess_data.preprocessing_data_for_activities_cheating()
        ## lag features
        lag_times = constant_num.lag_times
        lag_started_pos = constant_num.lag_started_pos_act
        lag_col = lag_cols.lag_cols_act
        lag_df = create_lag_features(lag_times, df, lag_col, lag_started_pos)
        # prediction
        prediction = list(detect_activities_model.predict(lag_df))
        counts = {}
        classes = classes_name.act_classes
        for i in classes:
            counts[i] = prediction.count(i)

        return make_response(
            jsonify(counts),
            200
        )
    except Exception as ex:
        print(''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__)))
    return 'ERROR'


@app.route('/detect_devices', methods=['GET'])
def detect_devices():
    '''
    input: data of 2 devices in json raw
    return: 0 if 2 devices not belongs to 1 person
            1 if 2 devices belongs to 1 person
    '''
    try:
        data = request.get_json()
        acceler1_df = pd.DataFrame(data['acceler1'])
        gyro1_df = pd.DataFrame(data['gyro1'])
        acceler2_df = pd.DataFrame(data['acceler2'])
        gyro2_df = pd.DataFrame(data['gyro2'])
        # preprocessing
        nbins = constant_num.nbins
        acceler1 = read_device_data(acceler1_df, nbins)
        gyro1 = read_device_data(gyro1_df, nbins)
        sensor1 = combine_device_data(acceler1, gyro1, nbins)
        acceler2 = read_device_data(acceler2_df, nbins)
        gyro2 = read_device_data(gyro2_df, nbins)
        sensor2 = combine_device_data(acceler2, gyro2, nbins)
        sensor = combine_device_data(sensor1, sensor2, nbins)
        preprocess_data = preprocessing_data(sensor)
        df = preprocess_data.preprocessing_data_for_devices_cheating()
        ## lag features
        lag_times = constant_num.lag_times
        lag_started_pos = constant_num.lag_started_pos_dev
        lag_col = lag_cols.lag_cols_dev
        lag_df = create_lag_features(lag_times, df, lag_col, lag_started_pos)
        # prediction
        prediction = list(detect_numofdevices_model.predict(lag_df))
        counts = {}
        classes = classes_name.dev_classes
        for i in classes:
            counts[i] = prediction.count(i)
        return make_response(jsonify(counts), 200)
    except Exception as ex:
        print(''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__)))
    return 'ERROR'


if __name__ == "__main__":
    app.run(debug=True)
