import numpy as np
import pandas as pd

class classes_name:
    act_classes = ['std', 'jog', 'wlk', 'ske']
    dev_classes = [0, 1]

class lag_cols:
    lag_cols_act = ['AccelerNorm', 'GyroNorm']
    lag_cols_dev = ['AccelerX1', 'AccelerX2', 'AccelerY1', 'AccelerY2', 'AccelerZ1',
             'AccelerZ2', 'AccelerNorm1', 'AccelerNorm2', 'GyroX1', 'GyroX2',
             'GyroY1', 'GyroY2', 'GyroZ1', 'GyroZ2', 'GyroNorm1', 'GyroNorm2',
             'Scaled_time1', 'Scaled_time2']

class constant_num:
    nbins = 10
    lag_times = 50
    lag_started_pos_act = 8
    lag_started_pos_dev = 16

class cols_pos:
    AccelerNorm_pos = 3
    GyroNorm_pos = 7
    AccelerNorm1_pos = 6
    AccelerNorm2_pos = 7
    GyroNorm1_pos = 14
    GyroNorm2_pos = 15

def read_device_data(df, nbins):
    df['Scaled_time'] = (df['DateTime'] - df['DateTime'].min())/(df['DateTime'].max()-df['DateTime'].min())
    df.sort_values('Scaled_time')
    df.loc[:,'time_count'] = pd.cut(df['Scaled_time'], nbins, retbins=True, labels=range(nbins))[0]
    return df

def combine_device_data(df_0, df_1, nbins):
    df_0_drop_indices_list = []
    df_1_drop_indices_list = []
    df_0 = df_0.loc[:,~df_0.columns.duplicated()]
    df_1 = df_1.loc[:,~df_1.columns.duplicated()]
    for i in range(nbins):
        k = len(df_0[df_0['time_count']==i]) - len(df_1[df_1['time_count']==i])
        if k==0:
            continue
        elif k>0:
            remove_n = k
            drop_indices_0 = np.random.choice(df_0[df_0['time_count']==i].index, remove_n, replace=False)
            df_0_drop_indices_list.extend(drop_indices_0)
        else:
            remove_n = -k
            drop_indices_1 = np.random.choice(df_1[df_1['time_count']==i].index, remove_n, replace=False)
            df_1_drop_indices_list.extend(drop_indices_1)

    df_0 = df_0.drop(df_0_drop_indices_list).reset_index()
    df_1 = df_1.drop(df_1_drop_indices_list).reset_index()
    df = pd.concat([df_0,df_1],axis=1)
    return df

class preprocessing_data:
    def __init__(self,data):
        self.data = data
    def preprocessing_data_for_activities_cheating(self):
        cols = ['AccelerX', 'AccelerY', 'AccelerZ', 'GyroX', 'GyroY', 'GyroZ']
        data = self.data[cols]
        data.insert(cols_pos.AccelerNorm_pos,'AccelerNorm',np.sqrt(data['AccelerX']**2 + data['AccelerY']**2 + data['AccelerZ']**2))
        data.insert(cols_pos.GyroNorm_pos,'GyroNorm',np.sqrt(data['GyroX']**2 + data['GyroY']**2 + data['GyroZ']**2))
        return data
    def preprocessing_data_for_devices_cheating(self):
        cols = ['AccelerX', 'AccelerY', 'AccelerZ', 'GyroX', 'GyroY', 'GyroZ', 'Scaled_time']
        data = self.data[cols]
        data.columns = ['AccelerX1', 'AccelerX2', 'AccelerY1', 'AccelerY2', 'AccelerZ1', 'AccelerZ2',
                        'GyroX1', 'GyroX2', 'GyroY1', 'GyroY2', 'GyroZ1', 'GyroZ2', 'Scaled_time1',
                        'Scaled_time2']
        cols = ['AccelerX1', 'AccelerX2', 'AccelerY1', 'AccelerY2', 'AccelerZ1',
                'AccelerZ2', 'GyroX1', 'GyroX2', 'GyroY1', 'GyroY2', 'GyroZ1', 'GyroZ2',
                'Scaled_time1', 'Scaled_time2']
        data = data[cols]
        data.insert(cols_pos.AccelerNorm1_pos, 'AccelerNorm1',
                    np.sqrt(data['AccelerX1'] ** 2 + data['AccelerY1'] ** 2 + data['AccelerZ1'] ** 2))
        data.insert(cols_pos.AccelerNorm2_pos, 'AccelerNorm2',
                    np.sqrt(data['AccelerX2'] ** 2 + data['AccelerY2'] ** 2 + data['AccelerZ2'] ** 2))
        data.insert(cols_pos.GyroNorm1_pos, 'GyroNorm1',
                    np.sqrt(data['GyroX1'] ** 2 + data['GyroY1'] ** 2 + data['GyroZ1'] ** 2))
        data.insert(cols_pos.GyroNorm2_pos, 'GyroNorm2',
                    np.sqrt(data['GyroX2'] ** 2 + data['GyroY2'] ** 2 + data['GyroZ2'] ** 2))
        return data

def create_lag_features(lag_features, df, cols,lag_started_pos):
    lag_df = df.copy()
    for j in cols:
        for i in range(lag_features):
            lag_df.insert(lag_started_pos+i, j + '_lag' + str(i+1), lag_df[j].shift(i+1))
    for i in range(lag_features):
        lag_df = lag_df.drop([i])
    return lag_df
